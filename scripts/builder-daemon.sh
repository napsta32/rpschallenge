#!/bin/bash

source ./scripts/docker-utils.sh

create_volume java_builder_volume
create_network java_builder_network
kill_container java_builder_daemon
docker build -t java-builder:latest ./docker/java-builder
docker run -d -p 8010:8000 --name java_builder_daemon --network java_builder_network -v java_builder_volume:/usr/src java-builder:latest
