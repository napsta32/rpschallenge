#!/bin/bash

function create_volume {
    local current_name=$(docker volume inspect --format '{{.Name}}' $1 2>/dev/null)
    if [ "${current_name}" == "$1" ]; then
        echo "Volume '${current_name}' already exists"
    else
        local new_name=$(docker volume create "$1")
        echo "Volume '${new_name}' created"
    fi
}

function create_network {
    local current_name=$(docker network inspect --format '{{.Name}}' $1 2>/dev/null)
    if [ "${current_name}" == "$1" ]; then
        echo "Network '${current_name}' already exists"
    else
        local new_name=$(docker network create "$1")
        echo "Network '${new_name}' created"
    fi
}

function kill_container {
    local is_dead=$(docker container inspect --format '{{.State.Dead}}' $1 2>/dev/null)
    if [ "${is_dead}" == "false" ]; then
        docker container stop "$1"
        docker container rm "$1"
        echo "Container $1 stopped"
    else
        echo "Container $1 is already down"
    fi
}