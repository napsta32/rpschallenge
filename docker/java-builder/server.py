from typing import Optional
import subprocess
from fastapi import FastAPI

app = FastAPI()
build_process: subprocess.Popen[str] = None


def is_ready() -> bool:
    global build_process
    if not build_process is None and not build_process.poll() is None:
        build_process = None
    return build_process is None


@app.get("/")
def read_root():
    return {"status": "ON"}


@app.get("/build")
def start_build():
    global build_process
    if is_ready():
        build_process = subprocess.Popen(['./gradlew', 'build'], cwd='/usr/src')
        return {"status": "BUILDING"}
    else:
        return {"status": "BUSY"}

@app.get("/ready")
def read_ready():
    return {"build_ready": is_ready()}