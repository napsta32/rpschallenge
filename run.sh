#!/bin/sh

BUILD_READY="{\"build_ready\":true}"

rm -rfv /usr/builder/*
cp -r . /usr/builder
curl java_builder_daemon:8000/build
until [ "$(curl -s java_builder_daemon:8000/ready)" = "${BUILD_READY}" ]
do
    sleep 1
    echo "Waiting build..."
done
rm -rf ./rpschallenge-0.0.1-SNAPSHOT.jar
cp /usr/builder/build/libs/rpschallenge-0.0.1-SNAPSHOT.jar ./rpschallenge-0.0.1-SNAPSHOT.jar
java -jar ./rpschallenge-0.0.1-SNAPSHOT.jar