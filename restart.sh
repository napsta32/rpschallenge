#!/bin/bash

docker-compose stop server
printf "y\n" | docker-compose rm server
docker-compose build server
RPSCHALLENGE_MYSQL_ROOT_PASSWORD=pass docker-compose up -d
