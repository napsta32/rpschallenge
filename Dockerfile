FROM openjdk:8

COPY . /usr/src/rpsapp
WORKDIR /usr/src/rpsapp

CMD [ "./run.sh" ]
