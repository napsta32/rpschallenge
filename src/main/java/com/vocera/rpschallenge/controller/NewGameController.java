package com.vocera.rpschallenge.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vocera.rpschallenge.model.Game;
import com.vocera.rpschallenge.model.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class NewGameController {

    @Autowired
    private ObjectMapper mapper;
    private final GameRepository repository;

    protected NewGameController(GameRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/start")
    JsonNode createNewGame() {
        String token = UUID.randomUUID().toString();
        Game game = new Game(token);
        return repository.save(game).getStartJson(this.mapper);
    }

}
