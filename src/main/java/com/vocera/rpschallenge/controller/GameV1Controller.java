package com.vocera.rpschallenge.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
// import com.fasterxml.jackson.databind.node.ObjectNode;
// import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vocera.rpschallenge.game.Hand;
import com.vocera.rpschallenge.game.Paper;
import com.vocera.rpschallenge.game.Rock;
import com.vocera.rpschallenge.game.Scissors;
import com.vocera.rpschallenge.model.ApiErrorException;
import com.vocera.rpschallenge.model.Game;
import com.vocera.rpschallenge.model.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class GameV1Controller {

    private static final Logger log = LoggerFactory.getLogger(GameV1Controller.class);
    private static final ApiErrorException TOKEN_NOT_FOUND_ERROR =
            new ApiErrorException(HttpStatus.NOT_FOUND, "Token not found");

    @Autowired
    private ObjectMapper mapper;
    private final GameRepository repository;

    protected GameV1Controller(GameRepository repository) {
        this.repository = repository;
    }

    private Hand getRandomHand() throws ApiErrorException {
        final Hand[] hands = {new Rock(), new Paper(), new Scissors()};
        long currentTime = System.currentTimeMillis();
        Random rng = new Random(currentTime);
        int randomDecision = Math.abs(rng.nextInt()) % hands.length;

        return hands[randomDecision];
    }

    @GetMapping("/v1/{token}/rock")
    JsonNode userPlaysRock(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            log.info("User does not exist");
            throw TOKEN_NOT_FOUND_ERROR;
        }
        log.info("User plays rock");
        JsonNode response = game.getUpdatedGameJson(getRandomHand(), new Rock(), mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

    @GetMapping("/v1/{token}/paper")
    JsonNode userPlaysPaper(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            log.info("User does not exist");
            throw TOKEN_NOT_FOUND_ERROR;
        }
        log.info("User plays paper");
        JsonNode response = game.getUpdatedGameJson(getRandomHand(), new Paper(), mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

    @GetMapping("/v1/{token}/scissors")
    JsonNode userPlaysScissors(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            log.info("User does not exist");
            throw TOKEN_NOT_FOUND_ERROR;
        }
        log.info("User plays scissors");
        JsonNode response = game.getUpdatedGameJson(getRandomHand(), new Scissors(), mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

}
