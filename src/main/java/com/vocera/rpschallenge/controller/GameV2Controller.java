package com.vocera.rpschallenge.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vocera.rpschallenge.game.Hand;
import com.vocera.rpschallenge.game.Paper;
import com.vocera.rpschallenge.game.Rock;
import com.vocera.rpschallenge.game.Scissors;
import com.vocera.rpschallenge.model.ApiErrorException;
import com.vocera.rpschallenge.model.Game;
import com.vocera.rpschallenge.model.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class GameV2Controller {

    private static final ApiErrorException TOKEN_NOT_FOUND_ERROR =
            new ApiErrorException(HttpStatus.NOT_FOUND, "Token not found");

    @Autowired
    private ObjectMapper mapper;
    private final GameRepository repository;

    protected GameV2Controller(GameRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/v2/{token}/rock")
    JsonNode userPlaysRock(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            throw TOKEN_NOT_FOUND_ERROR;
        }
        Hand userHand = new Rock();
        JsonNode response = game.getUpdatedGameJson(userHand.getBetterHand(), userHand, mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

    @GetMapping("/v2/{token}/paper")
    JsonNode userPlaysPaper(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            throw TOKEN_NOT_FOUND_ERROR;
        }
        Hand userHand = new Paper();
        JsonNode response = game.getUpdatedGameJson(userHand.getBetterHand(), userHand, mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

    @GetMapping("/v2/{token}/scissors")
    JsonNode userPlaysScissors(@PathVariable String token) throws ApiErrorException {
        Game game = repository.findByToken(token);
        if (game == null) {
            throw TOKEN_NOT_FOUND_ERROR;
        }
        Hand userHand = new Scissors();
        JsonNode response = game.getUpdatedGameJson(userHand.getBetterHand(), userHand, mapper);
        repository.save(game);

        // If no errors happen, return game response
        return response;
    }

}
