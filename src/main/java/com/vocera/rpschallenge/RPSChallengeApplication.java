package com.vocera.rpschallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RPSChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RPSChallengeApplication.class, args);
	}

}
