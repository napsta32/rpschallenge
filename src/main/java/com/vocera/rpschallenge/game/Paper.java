package com.vocera.rpschallenge.game;

import com.vocera.rpschallenge.model.ApiErrorException;
import org.springframework.http.HttpStatus;

public class Paper extends Hand {

    @Override
    public int compareTo(Hand hand) {
        if (hand instanceof Rock) {
            return 1;
        } else if (hand instanceof Paper) {
            return 0;
        } else if (hand instanceof Scissors) {
            return -1;
        }
        return 0;
    }

    @Override
    public String getName() {
        return "PAPER";
    }

    @Override
    public Hand getBetterHand() {
        return new Scissors();
    }
}
