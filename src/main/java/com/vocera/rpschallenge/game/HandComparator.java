package com.vocera.rpschallenge.game;

import java.util.Comparator;

public class HandComparator implements Comparator<Hand> {

    @Override
    public int compare(Hand firstHand, Hand secondHand) {
        return firstHand.compareTo(secondHand);
    }

}
