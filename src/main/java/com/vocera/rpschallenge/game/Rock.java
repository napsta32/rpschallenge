package com.vocera.rpschallenge.game;

public class Rock extends Hand {

    @Override
    public int compareTo(Hand hand) {
        if (hand instanceof Rock) {
            return 0;
        } else if (hand instanceof Paper) {
            return -1;
        } else if (hand instanceof Scissors) {
            return 1;
        }
        return 0;
    }

    @Override
    public String getName() {
        return "ROCK";
    }

    @Override
    public Hand getBetterHand() {
        return new Paper();
    }

}
