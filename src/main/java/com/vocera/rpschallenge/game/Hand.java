package com.vocera.rpschallenge.game;

import com.vocera.rpschallenge.model.ApiErrorException;

public abstract class Hand implements Comparable<Hand> {

    public abstract String getName();
    public abstract Hand getBetterHand();

}
