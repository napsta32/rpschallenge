package com.vocera.rpschallenge.game;

public class Scissors extends Hand {

    @Override
    public int compareTo(Hand hand) {
        if (hand instanceof Rock) {
            return -1;
        } else if (hand instanceof Paper) {
            return 1;
        } else if (hand instanceof Scissors) {
            return 0;
        }
        return 0;
    }

    @Override
    public String getName() {
        return "SCISSORS";
    }

    @Override
    public Hand getBetterHand() {
        return new Rock();
    }

}
