package com.vocera.rpschallenge.model;

import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Long> {

    Game findByToken(String token);

}
