package com.vocera.rpschallenge.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vocera.rpschallenge.game.Hand;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "game")
public class Game {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String token;
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer userScore;
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer serverScore;

    protected Game() {}

    public Game(String token) {
        this.token = token;
        this.userScore = 0;
        this.serverScore = 0;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, token='%s', userScore='%d', serverScore='%d']",
                id, token, userScore, serverScore);
    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public Integer getUserScore() {
        return userScore;
    }

    public Integer getServerScore() {
        return serverScore;
    }

    public void increaseUserScore() {
        this.userScore += 1;
    }

    public void increaseServerScore() {
        this.serverScore += 1;
    }

    public JsonNode getStartJson(ObjectMapper mapper) {
        ObjectNode node = mapper.createObjectNode();
        node.put("status", "READY");
        node.put("token", getToken());
        return node;
    }

    public JsonNode getUpdatedGameJson(Hand serverHand, Hand userHand, ObjectMapper mapper) {
        String status = null;
        boolean ignoreServerMove = false;

        if (getServerScore() == 3 || getUserScore() == 3) {
            status = "GAME OVER";
            ignoreServerMove = true;
        } else if (serverHand.compareTo(userHand) > 0) {
            increaseServerScore();
            status = "LOSS";
        } else if (userHand.compareTo(serverHand) > 0) {
            increaseUserScore();
            status = "WIN";
        } else {
            status = "TIE";
        }

        ObjectNode node = mapper.createObjectNode();
        node.put("status", status);
        if (!ignoreServerMove) {
            node.put("serverMove", serverHand.getName());
        }
        node.put("userScore", getUserScore());
        node.put("serverScore", getServerScore());
        return node;
    }

}
