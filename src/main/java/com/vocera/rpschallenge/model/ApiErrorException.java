package com.vocera.rpschallenge.model;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ApiErrorException extends Exception {

    private ApiError apiError;

    public ApiErrorException(HttpStatus status, String message, List<String> errors) {
        super(message);
        this.apiError = new ApiError(status, message, errors);
    }

    public ApiErrorException(HttpStatus status, String message, String error) {
        super(message);
        this.apiError = new ApiError(status, message, error);
    }

    public ApiErrorException(HttpStatus status, String message) {
        super(message);
        this.apiError = new ApiError(status, message, message);
    }

    public ApiError getApiError() {
        return apiError;
    }
}
